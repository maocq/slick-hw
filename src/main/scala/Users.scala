import slick.driver.MySQLDriver.api._

class Users(tag: Tag) extends Table[(Int, String, String)](tag, "USERS") {
  def id = column[Int]("ID", O.PrimaryKey) // This is the primary key column
  def name = column[String]("NAME")
  def email = column[String]("EMAIL")
  // Every table needs a * projection with the same type as the table's type parameter
  def * = (id, name, email)
}