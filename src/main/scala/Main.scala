import com.typesafe.config.ConfigFactory
import slick.driver.MySQLDriver.api._

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

object Main {

  def main(args: Array[String]): Unit = {

    val dbUrl = ConfigFactory.load().getString("db.default.url")
    val driver = ConfigFactory.load().getString("db.default.driver")
    val user = ConfigFactory.load().getString("db.default.username")
    val password = ConfigFactory.load().getString("db.default.password")
    val db = Database.forURL(dbUrl, driver = driver, user = user, password = password)

    try {

      val users = TableQuery[Users]

      val setup = DBIO.seq(

        (users.schema).create,

        users += (101, "Acme, Inc. =)",   "myemail@gmail.com"),
        users += ( 49, "Superior Coffee", "myemail@hotmail.com"),
        users += (150, "The High Ground", "myemail@yaho.es")
        // insert into USERS(USER_ID, USER_NAME, EMAIL) values (?,?,?)
      )
      //val setupFuture = db.run(setup)
      //Await.result(db.run(setup), Duration.Inf)


      val q1 = users.map(_.name)

      val action = q1.result
      val result: Future[Seq[String]] = db.run(action)
      val sql = action.statements.head


      Await.result(db.run(users.result), Duration.Inf).foreach {
        case (id, name, email) =>
          println("  " + name + "\t" + id + "\t" + email)
      }

      println("End")

    } finally db.close

  }

}
